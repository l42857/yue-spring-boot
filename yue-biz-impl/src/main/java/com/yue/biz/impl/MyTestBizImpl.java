package com.yue.biz.impl;

import com.yue.biz.MyTestBiz;
import com.yue.dao.mapper.MyTestMapper;
import com.yue.entity.MyTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class MyTestBizImpl implements MyTestBiz {
    @Autowired
    private MyTestMapper myTestMapper;
    @Override
    public List<MyTest> getAll() {
        return myTestMapper.selectAll();
    }
}
