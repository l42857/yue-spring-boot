package com.yue.web;

import com.yue.biz.MyTestBiz;
import com.yue.entity.MyTest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/myTestCtl")
@Api(tags ="测试")
public class MyTestCtl {

    @Autowired
    private MyTestBiz myTestBizImpl;
    @ApiOperation(value = "测试",notes ="测试")
    @GetMapping("/list")
    public List<MyTest> list(){
        return myTestBizImpl.getAll();
    }
}
