package com.yue.entity;

import lombok.Data;

import javax.persistence.Table;

@Data
@Table(name = "MY_TEST")
public class MyTest {

    private String id;
    private String name;
}
