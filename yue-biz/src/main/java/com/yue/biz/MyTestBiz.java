package com.yue.biz;

import com.yue.entity.MyTest;

import java.util.List;

public interface MyTestBiz {

    List<MyTest> getAll();
}
