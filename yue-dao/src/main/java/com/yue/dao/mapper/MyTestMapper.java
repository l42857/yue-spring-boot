package com.yue.dao.mapper;

import com.yue.entity.MyTest;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface MyTestMapper extends Mapper<MyTest> {

}
